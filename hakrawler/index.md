---
Title: hakrawler
Homepage: https://github.com/hakluke/hakrawler
Repository: https://gitlab.com/kalilinux/packages/hakrawler
Architectures: any
Version: 2.1-0kali1
Metapackages: kali-linux-everything kali-tools-web 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### hakrawler
 
  Fast golang web crawler for gathering URLs and JavaSript file locations.
  This is basically a simple implementation of the awesome Gocolly
  library.
 
 **Installed size:** `9.37 MB`  
 **How to install:** `sudo apt install hakrawler`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### hakrawler
 
 
 ```
 root@kali:~# hakrawler --help
 Usage of hakrawler:
   -d int
     	Depth to crawl. (default 2)
   -h string
     	Custom headers separated by two semi-colons. E.g. -h "Cookie: foo=bar;;Referer: http://example.com/" 
   -insecure
     	Disable TLS verification.
   -json
     	Output as JSON.
   -proxy string
     	Proxy URL. E.g. -proxy http://127.0.0.1:8080
   -s	Show the source of URL based on where it was found. E.g. href, form, script, etc.
   -size int
     	Page size limit, in KB. (default -1)
   -subs
     	Include subdomains for crawling.
   -t int
     	Number of threads to utilise. (default 8)
   -timeout int
     	Maximum time to crawl each URL from stdin, in seconds. (default -1)
   -u	Show only unique urls.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
