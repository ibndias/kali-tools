---
Title: pspy
Homepage: https://github.com/DominicBreuker/pspy
Repository: https://gitlab.com/kalilinux/packages/pspy
Architectures: amd64 i386
Version: 1.2.1-0kali1
Metapackages: 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### pspy
 
  pspy is a command line tool designed to snoop on processes without need for
  root permissions. It allows you to see commands run by other users, cron jobs,
  etc. as they execute. Great for enumeration of Linux systems in CTFs. Also
  great to demonstrate your colleagues why passing secrets as arguments on the
  command line is a bad idea.
 
 **Installed size:** `9.21 MB`  
 **How to install:** `sudo apt install pspy`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### pspy
 
 
 ```
 root@kali:~# pspy -h
 pspy - version: 1.2.1 - Commit SHA: kali
 
 
      ██▓███    ██████  ██▓███ ▓██   ██▓
     ▓██░  ██▒▒██    ▒ ▓██░  ██▒▒██  ██▒
     ▓██░ ██▓▒░ ▓██▄   ▓██░ ██▓▒ ▒██ ██░
     ▒██▄█▓▒ ▒  ▒   ██▒▒██▄█▓▒ ▒ ░ ▐██▓░
     ▒██▒ ░  ░▒██████▒▒▒██▒ ░  ░ ░ ██▒▓░
     ▒▓▒░ ░  ░▒ ▒▓▒ ▒ ░▒▓▒░ ░  ░  ██▒▒▒ 
     ░▒ ░     ░ ░▒  ░ ░░▒ ░     ▓██ ░▒░ 
     ░░       ░  ░  ░  ░░       ▒ ▒ ░░  
                    ░           ░ ░     
                                ░ ░
 
 Usage:
   pspy [flags]
 
 Flags:
   -c, --color                        color the printed events (default true)
       --debug                        print detailed error messages
   -d, --dirs stringArray             watch these dirs
   -f, --fsevents                     print file system events to stdout
   -h, --help                         help for pspy
   -i, --interval int                 scan every 'interval' milliseconds for new processes (default 100)
       --ppid                         record process ppids
   -p, --procevents                   print new processes to stdout (default true)
   -r, --recursive_dirs stringArray   watch these dirs recursively (default [/usr,/tmp,/etc,/home,/var,/opt])
   -t, --truncate int                 truncate process cmds longer than this (default 2048)
 ```
 
 - - -
 
 ##### pspy-binaries
 
 
 ```
 root@kali:~# pspy-binaries -h
 
 > pspy ~ Monitor Linux processes without root permissions
 
 /usr/share/pspy
 |-- pspy32
 |-- pspy32s
 |-- pspy64
 `-- pspy64s
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
